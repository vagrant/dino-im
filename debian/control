Source: dino-im
Maintainer: Debian XMPP Maintainers <pkg-xmpp-devel@lists.alioth.debian.org>
Uploaders: Martin <debacle@debian.org>,
           Taowa <taowa@debian.org>
Section: net
Priority: optional
Build-Depends: debhelper-compat (= 13),
	cmake,
	dh-exec,
	gettext,
	libgcrypt20-dev,
	libgee-0.8-dev,
	libglib2.0-dev,
	libgpgme-dev,
	libgspell-1-dev,
	libgstreamer-plugins-base1.0-dev,
	libgstreamer1.0-dev,
	libgtk-3-dev (>= 3.22),
	libnice-dev,
	libnotify-dev,
	libqrencode-dev,
	libsignal-protocol-c-dev (>= 2.3.2~),
	libsoup-3.0-dev,
	libsqlite3-dev,
	libsrtp2-dev,
	libwebrtc-audio-processing-dev,
	ninja-build,
	valac,
Standards-Version: 4.1.4
Rules-Requires-Root: no
Homepage: https://github.com/dino/dino
Vcs-Browser: https://salsa.debian.org/xmpp-team/dino-im
Vcs-Git: https://salsa.debian.org/xmpp-team/dino-im.git

Package: dino-im
Architecture: any
Depends: ${misc:Depends},
	${shlibs:Depends},
	dino-im-common (= ${source:Version}),
	gstreamer1.0-gtk3,
	gstreamer1.0-plugins-good,
Recommends: ca-certificates,
	dbus,
	fonts-noto-color-emoji,
	network-manager
Breaks: dino-im-common (<< 0.1.0-5~)
Replaces: dino-im-common (<< 0.1.0-5~)
Description: modern XMPP client
 Dino is a modern XMPP/Jabber client with a nice and clean look.
 It does support OMEMO and OpenPGP for end-to-end encryption
 and encrypted audio/video calls between two or more people.
 If you are looking for a Conversations look-alike, this
 program might be for you.

Package: dino-im-common
Architecture: all
Multi-Arch: foreign
Depends: ${misc:Depends}
Breaks: dino-im (<< 0.1.0-5~)
Description: modern XMPP client - common files
 Dino is a modern XMPP/Jabber client with a nice and clean look.
 It does support OMEMO and OpenPGP for end-to-end encryption
 and encrypted audio/video calls between two or more people.
 If you are looking for a Conversations look-alike, this
 program might be for you.
 .
 This package contains platform independent files.
